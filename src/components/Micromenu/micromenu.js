import React from 'react';
import firebase from 'firebase';
import {dbo} from '../firebase';

function goOut(e){ 
    e.preventDefault(); 
    firebase.auth(dbo).signOut();
    window.location.href="/";
    }
function Micromenu(props) { 
    return (
        <>
        <ul id="menuUser">
            <p>Hola: {props.nombre} </p>
        <li id="SubirLogo" title="subir archivo"><a href="/subirlogo" alt="subir logotipo"><img src={process.env.PUBLIC_URL +'upfile.svg'} alt="subir archivo" />Subir Logo</a></li>
          <li id="perfil"  title="perfil"><a href="/perfil" alt="perfil de usuario"><img src={process.env.PUBLIC_URL +'perfil.svg'} alt="perfil usuario" />Perfil</a></li>
          <li id="logOut" title="salir"><a onClick={goOut} href="/" alt="salir"><img src={process.env.PUBLIC_URL +'logut.svg'} alt="log out / salida" />Log Out</a></li>
         </ul> 
        </>
    )
}

export default Micromenu
