import React from 'react';
import '../SigPaso/SigPaso.css'
import homelogo from '../../img/home_logo.png';
 
function closeSegPAso(){
  window.location.href="/singin"
}

 
function SigPaso(dtUser) { 
    return (
        <>
          
   <div id="base_segPaso" onClick={closeSegPAso}>
  <div className="wrapper_segPaso">
    <span className="closeSegP" onClick={closeSegPAso}>+</span>
    <div className="cajaImg_seg_paso"><img src={process.env.PUBLIC_URL + 'background_segundoPaso.jpg'} alt="fsdf" /></div>
    <div className="cajaPersonal">
      <h2>Hola: <b>{dtUser.dat}</b> List@ Para Continuar.</h2>
    </div>
    <ul class="segPaso_opcciones">
      <li className="comp_perfil"><a href="/perfil" alt="ir al perfil"><img src={process.env.PUBLIC_URL + 'perfil.svg'} alt="fsf" />Completar perfil</a></li>
      <li className="subir_logo"><img src={process.env.PUBLIC_URL + 'upfile.svg'}  alt="asdf" />Subir Logo</li>
      <li className="segPAso_home"><a href="/" alt="ir a home"><img src={homelogo} alt="asfd" />Home</a></li>
    </ul>
  </div>
</div>
        </>
    )
}

export default SigPaso;
