import React, { useState, useEffect }  from 'react';
import Header from '../header';
import './Subirlogo.css'; 
import md5 from "md5";
import firebase from 'firebase/app';
import {dbo} from '../firebase'; 
 
  function Subirlogo(){ 
	
var RotaCharger = document.getElementById('chargerInfinite');
 var triggerResset;

          
			  
		  const[Lock, setLock] = useState(false);
		  const[segpsSVG, setsegpsSVG] = useState({ imageSVG: "" });
		  const[segpsPNG, setsegpsPNG] = useState({ imagePNG: "" });
		  const[segpsJPG, setsegpsJPG] = useState({ imageJPG: "" });
		  const[Datalogo, setDatalogo] = useState({ 
			  id_user: "",
			  nombre_logo: "",
			  descrip_logo: "",
			  miniature: "",  
			  pass:"" 
		  });
         
		  
 
							//-----=== Subir logos  ===------//
							
                         //--------=== filtro para logo SVG==-----------//
  function uploadLogosSVG(e){
	document.getElementById("Message2").innerHTML = ''; 
		let msg = document.getElementById("Message_step_2");
		let flInpt = document.getElementById(e.target.id);
		let paso2 = document.getElementsByClassName('two')[0];
		let dataImgSVG = e.target.files[0];
		let imgType = dataImgSVG.type;
		let imgSize = dataImgSVG.size;
		let striptype = imgType.split("/");
    if(striptype[0] === "image"){
		if(striptype[1] === "svg+xml"){
			if(imgSize < 1000200 ){  
				setsegpsSVG({...segpsSVG, imageSVG: {imgSVG:dataImgSVG } })
				flInpt.style.boxShadow = "";
				paso2.style.background = "white";
				msg.innerHTML =  ``;
			}else{
				msg.innerHTML =  `Solo imagenes con 1mgbt máximo `; 
				flInpt.style.boxShadow = "1px 2px 5px red";
				paso2.style.background = "red";
				e.target.value = "";
				setsegpsSVG({...segpsSVG, imageSVG: ""})
			}
			}else{
		msg.innerHTML =  `Solo esta permitido subir imagenes SVG en este input`;
		flInpt.style.boxShadow = "1px 2px 5px red"; 
		paso2.style.background = "red";
		e.target.value = "";
		setsegpsSVG({...segpsSVG, imageSVG: ""})
	  }
	}else{
		msg.innerHTML =  `Solo esta permitido subir imagenes`; 
		flInpt.style.boxShadow = "1px 2px 5px red"; 
		paso2.style.background = "red"; 
		e.target.value = "";
		setsegpsSVG({...segpsSVG, imageSVG: ""})
	}
  }
 
                      //--------=== filtro para logo PNG  ==-----------//

 
  function uploadLogosPNG(e){ 
	document.getElementById("Message2").innerHTML = ''; 
		let msg = document.getElementById("Message_step_2");
		let flInpt = document.getElementById(e.target.id);
		let paso2 = document.getElementsByClassName('two')[0];
		let dataImgSVG = e.target.files[0];
		let imgType = dataImgSVG.type;
		let imgSize = dataImgSVG.size;
		let striptype = imgType.split("/");
    if(striptype[0] === "image"){
		if(striptype[1] === "png"){
			if(imgSize < 1000200 ){  
				setsegpsPNG({...segpsPNG, imagePNG: {img:dataImgSVG } })
				flInpt.style.boxShadow = "";
				paso2.style.background = "white";
				msg.innerHTML =  ``;
			}else{
				msg.innerHTML =  `Solo imagenes con 1mgbt máximo `; 
				flInpt.style.boxShadow = "1px 2px 5px red";
				paso2.style.background = "red";
				e.target.value = "";
				setsegpsPNG({...segpsPNG, imagePNG: ""})
			}
			}else{
			msg.innerHTML =  `Solo esta permitido subir imagenes PNG en este input`;
			flInpt.style.boxShadow = "1px 2px 5px red"; 
			paso2.style.background = "red";
			e.target.value = "";
			setsegpsPNG({...segpsPNG, imagePNG: ""})
		}
	}else{
		msg.innerHTML =  `Solo esta permitido subir imagenes`; 
		flInpt.style.boxShadow = "1px 2px 5px red"; 
		paso2.style.background = "red"; 
		e.target.value = "";
		setsegpsPNG({...segpsPNG, imagePNG: ""})
	}
  }

                      //--------=== filtro para logo JPG  ==-----------//

  function uploadLogosJPG(e){ 
	document.getElementById("Message2").innerHTML = ''; 
	let msg = document.getElementById("Message_step_2");
	let flInpt = document.getElementById(e.target.id);
	let paso2 = document.getElementsByClassName('two')[0];
   let dataImgSVG = e.target.files[0];
   let imgType = dataImgSVG.type;
   let imgSize = dataImgSVG.size;
   let striptype = imgType.split("/");
    if(striptype[0] === "image"){
	if(striptype[1] === "jpeg"){
	if(imgSize < 1000200 ){  
		setsegpsJPG({...segpsJPG, imageJPG: {img:dataImgSVG } })
		flInpt.style.boxShadow = "";
		paso2.style.background = "white";
		msg.innerHTML =  ``;
		}else{
			msg.innerHTML =  `Solo imagenes con 1mgbt máximo `; 
			flInpt.style.boxShadow = "1px 2px 5px red";
			paso2.style.background = "red";
			e.target.value = "";
			setsegpsJPG({...segpsJPG, imageJPG: ""})
		}
	}else{
	msg.innerHTML =  `Solo esta permitido subir imagenes JPG en este input`;
	flInpt.style.boxShadow = "1px 2px 5px red"; 
	paso2.style.background = "red";
	e.target.value = "";
	setsegpsJPG({...segpsJPG, imageJPG: ""})
}
   }else{
	msg.innerHTML =  `Solo esta permitido subir imagenes`; 
	flInpt.style.boxShadow = "1px 2px 5px red"; 
	paso2.style.background = "red"; 
	e.target.value = "";
	setsegpsJPG({...segpsJPG, imageJPG: ""})
   }
    // if(dataImg)
  }

							   //--------=== filtro para miniatura==-----------//
 
  function miniature(e){ 
		let msg = document.getElementById("Message");
		let flInpt = document.getElementById(e.target.id);
		let paso1 = document.getElementsByClassName('one')[0];
		let dataImg = e.target.files[0];
		let imgType = dataImg.type;
		let imgSize = dataImg.size;
		let striptype = imgType.split("/");
   if(striptype[0] === "image"){
	if(striptype[1] === "jpeg" || striptype[1] === "svg+xml"  || striptype[1] === "png"){
	if(imgSize < 1000200 ){ 
			  setDatalogo({...Datalogo, miniature: {img:dataImg } })
			flInpt.style.boxShadow = "";
			paso1.style.background = "white";
			msg.innerHTML =  ``;
	}else{
		msg.innerHTML =  `Solo imagenes con 1mgbt máximo `; 
		flInpt.style.boxShadow = "1px 2px 5px red";
		paso1.style.background = "red";
		e.target.value = "";
		  setDatalogo({...Datalogo, miniature: "" })
	}
	}else{
	msg.innerHTML =  `Solo esta permitido subir imagenes jpg/svg/png`;
	flInpt.style.boxShadow = "1px 2px 5px red"; 
	paso1.style.background = "red";
	e.target.value = "";
	  setDatalogo({...Datalogo, miniature: "" })
	}
   }else{
	msg.innerHTML =  `Solo esta permitido subir imagenes`; 
	flInpt.style.boxShadow = "1px 2px 5px red"; 
	paso1.style.background = "red"; 
	e.target.value = "";
	setDatalogo({...Datalogo, miniature: "" })
   }
  }
 
  
				  //------===== filtro para el nombre del logo ====-----//
				  

function nameLogo(e){
	let msg = document.getElementById("Message");
	let redBrd = document.getElementById(e.target.id);
	let paso1 = document.getElementsByClassName('one')[0];
	let digits = e.target.value; 
	let dign = digits.replace(" ","") 
	var re = /[<>=+/*%!?@#$`^&;:(")'}{-]/g;
	var result = re.test(dign);
		if(result !== true){
				if(  e.target.id === "nombre_logo"){
					setDatalogo({...Datalogo, nombre_logo: digits})  
				} else if(e.target.id === "log_desc"){
					setDatalogo({...Datalogo, descrip_logo: digits})  
				} 
				msg.innerHTML = "";
				paso1.style.background = "white";
				redBrd.style.boxShadow = "none"

				}else{
				if(  e.target.id === "nombre_logo"){
					setDatalogo({...Datalogo, nombre_logo: ""})  
				}else if(e.target.id === "log_desc"){
					setDatalogo({ ...Datalogo, descrip_logo: ""})  
				} 
		msg.innerHTML =  `No carácteres especiales <>,./?!@#$%^&*`; 
		paso1.style.background = "red";
		redBrd.style.boxShadow = "1px 2px 5px red"
	}
}


				  //------===== blocquear ====-----//


function changeLock(e){ 
	setDatalogo({...Datalogo, whitPassw:e.target.value})
	setLock(!Lock)
}
 
				  //------===== pass blocquear ====-----//


function passLock(e){
	let lc = e.target.value; 
	// var md5 = require('md5'); 
	 let hash = md5(lc);
	 setDatalogo({...Datalogo, pass:hash}) 
}
 
 function pasosResset(){
    const moving = document.getElementsByClassName('stepses')[0];
    moving.style.marginLeft = 0;
 }

 function pasoUno(e){
	 e.preventDefault();
    const contenedor = document.getElementsByClassName('wrapper_steps')[0].clientWidth;
     const moving = document.getElementsByClassName('stepses')[0];
         moving.style.marginLeft = '-' + contenedor + 'px';
 }
 function pasoDos(e){
	 e.preventDefault();
		const contenedor = document.getElementsByClassName('wrapper_steps')[0].clientWidth;
		const pasoDos = contenedor*2;
		const moving = document.getElementsByClassName('stepses')[0];
     moving.style.marginLeft = '-' + pasoDos + 'px';
 }



async function sabeDataOndatabase(){     
	
RotaCharger.style.display = 'flex';
let getNameLs = localStorage.getItem('fileOptipus');
const db = firebase.firestore(dbo);
const dbs = firebase.storage(dbo);
 await db.collection('CollectioLogotipo').add({ 
	id_user: getNameLs,
	Nombre_logo: Datalogo.nombre_logo,
	Descripccion: Datalogo.descrip_logo,
	block: Lock,
	pass: Datalogo.pass,
	like:0
  }).then(function(docRef) {  

	 
	 //-----------===== REINICIAR TODO EL FORM ======----------//
	const nombre = Datalogo.miniature.img.name
    const storag = dbs.ref(docRef.id+'/miniatura_'+nombre);
	const subio = storag.put(Datalogo.miniature.img);
	subio.on('state_changed',function(snapshot){  
		//---=== LA IMAGEN HA SUBIDO
		}, function(error){
		console.log(error);
		}, function(){
		dbs.ref(docRef.id+'/').child('miniatura_'+nombre).getDownloadURL()
		.then(ur=>{
		 db.collection('CollectioLogotipo').doc(docRef.id)
        .set({
			id: docRef.id,
			miniature:ur
		     },{merge:true}).then(result=>{ 
				 console.log("result luego de guardar los datos: ___", result); 
				setLock(false);
				document.getElementById("nombre_logo").value = ""; 
				document.getElementById("log_desc").value = "";
				document.getElementById("passLock").value = ""; 
				document.getElementById("block_logo").checked = false;

				setDatalogo({...Datalogo, miniature:"", id_user: "", nombre_logo: "", descrip_logo: "", pass: ""});
				document.getElementById("miniatura").value = "";
				RotaCharger.style.display = 'none';  
			 
				if(segpsSVG.imageSVG !== "" ){ 
				subirImgSVG(docRef.id, getNameLs)
				}else{console.log("la funcion de subir svg se ha interrumpido");}
			   
				if(segpsPNG.imagePNG !== ""){
					subirImgPNG(docRef.id, getNameLs) 
				}else{console.log("la funcion de subir png se ha interrumpido");}

				if(segpsJPG.imageJPG !== ""){
					subirImgJPG(docRef.id, getNameLs)  
				}else{console.log("la funcion de subir jpg se ha interrumpido");}

			
		}).catch(error=>{
		console.log("Resultados de un error al guardar: ",error)
	       })
		})
	}) 
	  //=== continuar con el proceso
     
})
.catch(function(error) {
    console.error("Error adding document: ", error);
}); 
}


                   // subir svg si exsite
function subirImgSVG(urlContainer, urlCollection){
	 
	RotaCharger.style.display = 'flex';
		const db = firebase.firestore(dbo);
		const dbs = firebase.storage(dbo);
		const nombre = segpsSVG.imageSVG.imgSVG.name
		const storag = dbs.ref(urlContainer+'/SVG_'+nombre);
		const subio = storag.put(segpsSVG.imageSVG.imgSVG);
	subio.on('state_changed',function(snapshot){  
		}, function(error){
		console.log(error);
		}, function(){
		dbs.ref(urlContainer+'/').child('SVG_'+nombre).getDownloadURL()
		.then(url=>{ 
		 db.collection('CollectioLogotipo').doc(urlContainer)
        .set({
			imageSVG:url
		},{merge:true}).then(result=>{ 
			pasosResset();
			document.getElementById("onlySVG").value = "";
			setsegpsSVG({imageSVG: ""})
			RotaCharger.style.display = 'none'; 
			}).catch(error=>{
				console.log("Resultados de un error al guardar: ",error)
			})

		})
 	})  
}



					// subir PNG si exsite  //
					
function subirImgPNG(urlContainer, urlCollection){
	 
	RotaCharger.style.display = 'flex';
	const db = firebase.firestore(dbo);
	const dbs = firebase.storage(dbo);
	const nombre = segpsPNG.imagePNG.img.name
	const storag = dbs.ref(urlContainer+'/PNG_'+nombre);
	const subio = storag.put(segpsPNG.imagePNG.img);
	   subio.on('state_changed',function(snapshot){
		 
		}, function(error){
		console.log(error);
		}, function(){
		dbs.ref(urlContainer+'/').child('PNG_'+nombre).getDownloadURL()
		.then(url=>{
			
			db.collection('CollectioLogotipo').doc(urlContainer)
		.set({
			imagePNG:url
		},{merge:true}).then(result=>{ 
			pasosResset();
			setsegpsPNG({ imagePNG: "" });
			document.getElementById("onlyPNG").value = "";
			RotaCharger.style.display = 'none';
			}).catch(error=>{
				console.log("Resultados de un error al guardar: ",error)
			})

		})
	})  
}


					 // subir JPG si exsite  //
					 
	function subirImgJPG(urlContainer, urlCollection){
		  
		RotaCharger.style.display = 'flex';
		const db = firebase.firestore(dbo);
		const dbs = firebase.storage(dbo);
		const nombre = segpsJPG.imageJPG.img.name
		const storag = dbs.ref(urlContainer+'/JPG_'+nombre);
		const subio = storag.put(segpsJPG.imageJPG.img);
		subio.on('state_changed',function(snapshot){
			}, function(error){
			console.log(error);
			}, function(){
			dbs.ref(urlContainer+'/').child('JPG_'+nombre).getDownloadURL()
			.then(url=>{
				
				db.collection('CollectioLogotipo').doc(urlContainer)
			.set({
				imageJPG:url
			},{merge:true}).then(result=>{ 
				pasosResset();
				document.getElementById("onlyJPG").value = "";
				setsegpsJPG({ imageJPG: "" });
				RotaCharger.style.display = 'none'; 
				}).catch(error=>{
					console.log("Resultados de un error al guardar: ",error)
				})
			})
		})  
	}






 function sendFulldata(e){ 
	 e.preventDefault(); 
	var msg = document.getElementById("Message");
	var msg2 = document.getElementById("Message2"); 
	if(Datalogo.nombre_logo !== ""){
		if(Datalogo.miniature !== ""){
			if(segpsSVG.imageSVG !== "" || segpsPNG.imagePNG !== "" || segpsJPG.imageJPG !== ""){
				sabeDataOndatabase();   //--276 
				msg2.innerHTML =  ``; 
			}else{
			let paso1 = document.getElementsByClassName('two')[0]; 
			msg2.innerHTML =  `Debe subir por lo menos un logo`; 
			paso1.style.background = "red"; 
			}     
		}else{
			// console.log('miniatura: esto no pede esta vacio');
			let redBrd = document.getElementById("miniatura");
			let paso1 = document.getElementsByClassName('one')[0]; 
			msg.innerHTML =  `Debe colocar una imagen que represente al logo`; 
			paso1.style.background = "red";
			redBrd.style.boxShadow = "1px 2px 5px red"

		}
	}else{// primera comprobacion
		let redBrd = document.getElementById("nombre_logo");
	     let paso1 = document.getElementsByClassName('one')[0]; 
		msg.innerHTML =  `Este imput no puede estar vacio`; 
		paso1.style.background = "red";
		redBrd.style.boxShadow = "1px 2px 5px red"
	}
 }//sendFulldata


 
 

   
 if(localStorage.getItem('fileOptipus') !== null){
    return(
        <>
		   <div id="chargerInfinite">
			   <div className="rotadorContainer">
				   <img id="rotadorRapido" src={process.env.PUBLIC_URL + 'bigRing.svg'} alt="rotadorRapido" />
				   <img id="rotadorLento" src={process.env.PUBLIC_URL + 'smallRing.svg'} alt="rotadorLento" />
			   </div>
			</div>
        <Header />
        <div id="uploadLogoSecction">
	<div className="wrapperUplogo">
	 
		<div id="steps"> 
			<div className="line"></div>
			<span className="one" onClick={pasosResset}>1</span>
			<span className="two" onClick={pasoUno}>2</span>
			<span className="tree" onClick={pasoDos}>3</span>
		</div>

		<div className="box_slier_masc">
			<div className="wrapper_steps">
			 
				<div className="stepses"> 
					<div className="steps_1">
						<code id="Message"></code>
						<span>Coloque el nombre de su logo, alguna descripción y una miniatura.</span> 
						
						<input onChange={nameLogo} type="text" placeholder="Nombre del logo" id="nombre_logo" />
						<textarea id="log_desc" onChange={nameLogo} placeholder="Si desea coloque los parametros 
						de uso del logo o una descripción del mismo"></textarea> 
						<input type="file" onChange={miniature}  id="miniatura"/>
						<button onClick={pasoUno}>Siguiente</button>
					</div>
					<div className="steps_2">
					<code id="Message_step_2"></code>
					<code id="Message2"></code>
						<span>Suba los formatos disponible de su logo svg/png/jpg</span>
						<input type="file" onChange={uploadLogosSVG} accept="svg" id="onlySVG" />
						<input type="file" onChange={uploadLogosPNG} accept="png"  id="onlyPNG" />
						<input type="file"  onChange={uploadLogosJPG} accept="jpg"  id="onlyJPG" />
						<button onClick={pasoDos}>Siguiente</button>
					</div>
					<div className="steps_3">
						<span>Seguridad/Finalizar</span>
						<span className="checkboxInfo">Colocar pasword? sin el no se podrá descargar el logo</span>
						<cite>Al palomear el checkbox hara que su logo no pueda ser descargable a menos que coloque
							un pasword y le sea revelado a quien desee descargar el logo
						</cite>
						<div className="wrapperPass"> 
						<input type="checkbox" onChange={changeLock} value={Lock} id="block_logo" /> 
						<input type="password" onChange={passLock}  placeholder="pasword" id="passLock" />
						 { Lock === true ? <img id="lock"src={process.env.PUBLIC_URL+"candado.svg"} alt="lock/unlock"/> : ""}
						</div>
						<button onClick={sendFulldata} id="envDatos">Subir logo</button>
					</div> 
				</div>
				 
			</div>
		</div> 
	</div>
</div>
        </>
	)
	}else{
		window.location.href = '/';
	}
  }
  export default Subirlogo;

  /*
  .       - Any Character Except New Line
\d      - Digit (0-9)
\D      - Not a Digit (0-9)
\w      - Word Character (a-z, A-Z, 0-9, _)
\W      - Not a Word Character
\s      - Whitespace (space, tab, newline)
\S      - Not Whitespace (space, tab, newline)

\b      - Word Boundary
\B      - Not a Word Boundary
^       - Beginning of a String
$       - End of a String

[]      - Matches Characters in brackets
[^ ]    - Matches Characters NOT in brackets
|       - Either Or
( )     - Group

Quantifiers:
*       - 0 or More
+       - 1 or More
?       - 0 or One
{3}     - Exact Number
{3,4}   - Range of Numbers (Minimum, Maximum)


#### Sample Regexs ####
https://www.youtube.com/watch?v=sa-TUpSx1JA
[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+ */