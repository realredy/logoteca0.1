import React, { useState, useEffect }  from 'react';
import logo from '../img/logoteca.svg'; 
import lupa from '../img/lupa.svg';
import loguin_icon from '../img/loguin.svg';
import singIn_icon from '../img/singIn.svg';
import './StyleHead.css';
import Loguin from './loguin/loguin';
import firebase from 'firebase';
import {dbo} from './firebase';
import Micromenu from './Micromenu/micromenu';
 
 

const divStyle = {  backgroundImage: 'url(' + lupa + ')' } 

        function showLoguin(){ 
        let loguinPanel = document.getElementById("loguinSection");
        loguinPanel.style.right="0px";

        }

        var hideUserMenu = true;
        let showUsermenu = ()=>{

        let menu = document.getElementById('menuUser');
        if(hideUserMenu === true){ 
        menu.style.right = 0;

        }else{
        menu.style.right = '-100%';
        }
        hideUserMenu = !hideUserMenu;
        // console.log(hideUserMenu);

        }



 


        function goSingIn(){ 
        document.location="/singin";
        }

        function search(){
        let ampl = document.getElementById('buscardor'); 
        ampl.style.width = '150px';  
        }

        function outfocus(){
        let ampl = document.getElementById('buscardor'); 
        ampl.addEventListener("focusout", function(){ 
        ampl.style.width = '78px';  
        });
        }

        document.addEventListener('click',()=>{
        outfocus();
        });


             var swichNotif = false;
        function showNotf(e){  
          console.log(hideUserMenu)
          
          let containerNotificaciones = document.getElementById('sistemaNotificaciones')
          
             if(containerNotificaciones === null){
               return  } else {

                 if(hideUserMenu === false){

                showUsermenu();
                    containerNotificaciones.style.right = 0;
                      swichNotif = !swichNotif;
                       
                    }else if(hideUserMenu === true && swichNotif === false){
                       containerNotificaciones.style.right = 0;  
                         swichNotif = !swichNotif;
                          
                          }else{
                            containerNotificaciones.style.right = '-100%'; 
                            swichNotif = !swichNotif;
                          }
               }
               
               
           
            
        }
         






 

function Header() {  


  
  const[nofificaciones, setnofificaciones] = useState()
  const[logued, setlogued] = useState(false);
  const[userdata, setuserdata] = useState({
                  nombre: '',
                  foto: '',
                  idUser: ''
  })



async function getDatos(){  
  try {   
  let containerNotificaciones = document.getElementById('sistemaNotificaciones'); 
  let db = firebase.firestore(dbo);
  let getingdaaa = db.collection("sistemLikes").where("creador_logo", "==", userdata.idUser); 
    let querySnapshot = await getingdaaa.get();
    if(querySnapshot.docs.length !== 0){ 
      setnofificaciones(true) 
      containerNotificaciones.innerHTML = '';
      querySnapshot.forEach(function(doc) {
        let actualData = doc.data().created.toDate();
        let dataatexto = actualData.toString()
        let dataShort = dataatexto.substr(0,25)
         
          setnofificaciones(true) 
          containerNotificaciones.innerHTML += `
          <div id="itemsNotificaciones" class="`+doc.id+`">
          <div id="closeBTN">+</div>
            <div class="imgNotfLogo">
              <img src="`+doc.data().logoImg+`" alt="`+doc.data().nombreLogo+`">
            </div>
            <div class="infoNotif">
              <ul>
                <li class="imgenquienLegusta">
                  <img src="`+doc.data().fotoUsuario+`" alt="foto del usuario">
                </li>
                <li class="aboutlike"><b>(`+doc.data().nombreLogo+`) Le gusta a: </b> <p>`+doc.data().nameLiker+`</p> <cite>en: `+dataShort+`</cite></li>
              </ul>
            </div>
      
        </div> `;

            
      });

              } else { 
               setnofificaciones(false)  
               containerNotificaciones.style.right = '-100%'; 
              }
           



    }  catch ( error)  {
      console.log("Error getting documents: ", error);
                       } 
   
}







let deletNof = (e)=>{  
  if(e.target.id == 'closeBTN'){
    let delettex = e.target.parentNode.classList[0];
    // let containerNotificaciones = document.getElementById('sistemaNotificaciones')
    const db = firebase.firestore(dbo);
      db.collection("sistemLikes").doc(delettex).delete().then(function() {
        
        // containerNotificaciones.innerHTML = ``;
          console.log("Document successfully deleted!");
          // setnofificaciones(false) 
            getDatos(); 

          }).catch(function(error) {
        console.error("Error removing document: ", error);
      }); 
   }
}




 
useEffect(()=>{  
  getDatos(); 
 }); 
 




 
  
  

useEffect(()=>{
  firebase.auth(dbo).onAuthStateChanged(function(user){  
    if(user){ 
  const db = firebase.firestore(dbo);
  const conex = db.collection('usuarios').doc(user.uid);
  conex.get().then(doc => {
    const dat = doc.data();
    setuserdata({nombre: dat.name,
                  foto: dat.img,
                  idUser: user.uid});
    localStorage.setItem("fileOptipus", user.uid);
    
        });
setlogued(true)

    } else { 
        localStorage.removeItem("fileOptipus");
        setlogued(false) 
    }
});
},[])


 


useEffect(()=>{
  // if(nofificaciones === false){
  //   let containerNotificaciones = document.getElementById('sistemaNotificaciones');
  //       containerNotificaciones.style.right = '-100%'; 
  // }
  getDatos();
},[nofificaciones])




let isl = ()=>{
  if(logued !== true){
    return(
      <>
      <li className="loguin" onClick={showLoguin}><img className="loguin_icon" src={loguin_icon} alt="loguin icon "/>loguin</li> 
        <li className="sing_in"  onClick={goSingIn}><img className="singInLogo" src={singIn_icon} alt="sing in logo" />sing in</li> 
      </>
    )
  }else{
    return(
      <> 
         <div onClick={showNotf} id="photoUser"  className={nofificaciones ? 'activeRing': ''}> 
         <img src={userdata.foto} alt="user foto" /> 
         </div> 

        <ul id="burguerMenu" onClick={showUsermenu}>
            <li className="tops"></li>
            <li className="middles"></li>
            <li className="bottoms"></li>
        </ul>
      </>
    )
  }
}
  
  return (
    <div className="Top_header">
        <div id="wrapper-header">
      <a href="/"><img src= {logo} className="headlogo" alt="logoteca logo"/></a>
      <ul className="menupr">
          <li><a href="/gridlogos">Colección</a></li>
          <li><a href="/">¿como subir tu logo?</a></li> 
      </ul>
      <input id="buscardor"  onFocus={search} name="buscador" type="text"  
      style={divStyle}/>
      <ul className="userSection"> 
           {isl()} 
      </ul>
     </div>
     {nofificaciones ? <div onClick={deletNof} id="sistemaNotificaciones">  </div>: '' } 
    {logued !== false ? <Micromenu nombre={userdata.nombre} /> : <Loguin />} 
    <ul className="bottom_menu">
          <li><a href="/gridlogos">Colección</a></li>
          <li><a href="/">¿como subir tu logo?</a></li> 
      </ul>
   </div>
  );
}

export default Header;
