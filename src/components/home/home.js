import React from 'react'
import Header from '../header'
import Slider from '../slider/slider'

function home() { 
    return (
        <>
        <Header />
        <Slider />
        </>
    )
}

export default home
