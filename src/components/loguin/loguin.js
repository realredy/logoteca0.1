 
import React from 'react';
import fond from '../../img/base_singin.jpg';
import './loguin.css';
import firebase from 'firebase';
  import {dbo} from '../firebase'; 

function Loguin() { 

    function hideLoguin(){
     
        let loguinPanel = document.getElementById("loguinSection");
        loguinPanel.style.right="-120%"; 
    }
    function formMakeLoguin(e){ 
        let msgBody = document.getElementsByClassName('error_mensaje')[0];
        let errMsg = document.getElementById('errorMessage');
        e.preventDefault();
        let mail = e.target.mail.value;
        let pass = e.target.pass.value;
       
        
        firebase.auth(dbo).setPersistence(firebase.auth.Auth.Persistence.SESSION)
        .then(()=>{
          return firebase.auth(dbo).signInWithEmailAndPassword(mail, pass)
          .then(user=>{ 
            if(user){ 
              hideLoguin();
                         /*  ---Obteniendo datos de la firebase----  */
                // const db = firebase.firestore(dbo);
                // const conex = db.collection('userSeccion').doc(user.user.uid);
                // conex.set({
                //     coneccionStatus: true
                // }).then(doc => { 
                // console.log(doc); 
                // });
    window.location.href="/";
            }
        })

        }).catch(err=>{
          console.log(err.message )
          msgBody.style.display="inherit";
          errMsg.innerHTML = err.message;
        setTimeout(()=>{
            msgBody.style.display="none";
          },7000) 
          
      })
    }

 

              /*  ---Obteniendo datos de la firebase----  */
    // const db = firebase.firestore(dbo);
    // const conex = db.collection('logoteca').doc('datalogoteca');
    // conex.get().then(doc => {
    // const dat = doc.data();
    // console.log(dat); 
    // });


    return (
        <div id="loguinSection">
            <b id="closeButom" onClick={hideLoguin}>+</b>
            <div className="wrapper_form_loguin">
             <img src={fond} alt='logo loguin'/>
             <span className="titleLoguin">
                 Coloque su email y password
             </span>
             <form onSubmit={formMakeLoguin}>
        <input id="nombre" type="email" name="mail" placeholder="Nombre" />
        <input id="pass" type="password" name="pass" placeholder="**********" /> 
             <input type="submit" value="Loguin" />
             <div className="error_mensaje">
                 <img src={process.env.PUBLIC_URL +'bad.png'} alt="error Mensaje" />
                 <span id="errorMessage"></span>
             </div>
             
             </form>
            </div>
        </div>
    )
}

export default Loguin;
