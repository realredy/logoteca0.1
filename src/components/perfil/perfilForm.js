import React, { useState, useEffect }  from 'react';
import Header from '../header';
import info from '../GridModule/img/info.jpg'
import jpg from '../GridModule/img/jpg.jpg'
import like from '../GridModule/img/like.jpg'
import lock from '../GridModule/img/lock.jpg'
import open from '../GridModule/img/open.jpg'
import png from '../GridModule/img/png.jpg'
import svg from '../GridModule/img/svg.jpg'
import './perfil.css'; 
import firebase from 'firebase/app';
import {dbo} from '../firebase';
      
 

       
   
var getNameLs = localStorage.getItem('fileOptipus');

  
function Perfilform (){
 


 const estylos = {
    darklines: {backgroundImage: 'url('+process.env.PUBLIC_URL + 'darklines.jpg)'},
    circles:   {backgroundImage: 'url('+process.env.PUBLIC_URL + 'circles.jpg)'},
    clear:     {backgroundImage: 'url('+process.env.PUBLIC_URL + 'clear.jpg)'},
    difuse:    {backgroundImage: 'url('+process.env.PUBLIC_URL + 'difuse.jpg)'},
    gaming:    {backgroundImage: 'url('+process.env.PUBLIC_URL + 'gaming.jpg)'},
    tech:      {backgroundImage: 'url('+process.env.PUBLIC_URL + 'tech.jpg)'}
}


    const [datos, setdatos] = useState({
          perfilTema: "",
          img: process.env.PUBLIC_URL + 'default_avatar.jpg',
          description: "",
          linkeding: "",
          facebook: "",
          twitter: "",
          website: ""
    });



   

   let getActionInPerfil = (e) => {
       console.log(e.target.tagName);
     
        if(e.target.tagName === "LI" && e.target.parentNode.children[1].id === "logDlt" && e.target.id === "logDlt"){

        ConfofirmProcessDeltLogo(e.target.classList[0]); 
        console.log("la clase es si cancela es: ",e.target.classList[0]); 
        }else{
            return 'undefined';
        }
    
          
         
    }

    let ConfofirmProcessDeltLogo = (deleteIts) => {
        if (window.confirm("Esta seguro de eliminar este logo")) { 
            const db = firebase.storage(dbo);
            const storag = db.ref(deleteIts);
            const st = db.ref(deleteIts); 
            let cantidadImagens; 
           
            storag.listAll().then(function(res) {
                  
                  switch (res.items.length ) {
                      case 2:
                        cantidadImagens = 25;
                          break;
                          case 3:
                            cantidadImagens = 16.6;
                              break;
                              case 4:
                            cantidadImagens = 12.5;
                              break;
                      default:                    break;
                  } 
                        
                res.items.forEach(function(itemRef) {  

                 
               executeDelete(cantidadImagens,res.items.length, st.fullPath,itemRef.name)
                  
                });
              }).catch(function(error) {
                // Uh-oh, an error occurred!
              }); 
                // console.log(getDlts);
             
        }
    }
       


    function executeDelete(porc, cant, st,datv){ 
        let barra = document.getElementById('progressBar');
        let db = firebase.storage(dbo);
        let storag = db.ref(st).child(datv);
        storag.delete().then(sa=>{ 
            barra.style.display = 'block';
            barra.style.width = porc * cant + '%';
             deleteDataLogo(barra, st);
        }).then(err=>{
            console.log('lanza un error',err);
        }) 
         console.log(st,"/",datv);   
    }



let deleteDataLogo = (barra, referencia) => {  
    let basegrid = document.getElementById('grid_box');  
    let hidebox = document.getElementById(referencia);  
     let actualbr = barra.style.width.split('%')[0];
    const db = firebase.firestore(dbo);
    const conex = db.collection('CollectioLogotipo').doc(referencia);
    const elimg =  conex.delete()
    elimg.then(datosCorr =>{ 
        // barra.style.width = parseInt(actualbr + 50)+ '%';
        barra.style.width = '100%';
        setTimeout(()=>{
            hidebox.style.display = 'none';
            setTimeout(()=>{ 
                barra.style.display = 'none';
                barra.style.width = '0%';
            },100);
        },300);
        
    })
    
}
               




     function accionEdita(){
        let perfilBox = document.getElementById('formulario_compl_perfil');
        let logoBox = document.getElementById('aportes');
    perfilBox.style.display = "block";
    logoBox.style.display = "none";
    } 

    function accionLogo(){
        let perfilBox = document.getElementById('formulario_compl_perfil');
        let logoBox = document.getElementById('aportes');
        perfilBox.style.display = "none";
        logoBox.style.display = "block";
    } 
 
         
// Colocar los datos al cargar el website
 async function chargeData(){
       let userName = document.getElementById('username');
        let tema = document.getElementById('backgroun_perfil');
        let desciption = document.getElementById('desc');
        let link = document.getElementById('linkeding'); 
        let faceblink = document.getElementById('facebooklink'); 
        let twiterlink = document.getElementById('twitterlink'); 
        let weblink = document.getElementById('websiteink');
        let getNameLs = localStorage.getItem('fileOptipus');
        let avatar = document.getElementById('AvatImg');
         
        const db = firebase.firestore(dbo);
        const conex = db.collection('usuarios').doc(getNameLs);
        const llamada = await  conex.get();
        const respuesta = llamada.data(); 
        userName.innerHTML = respuesta.name; 
        setdatos((respuesta))    
        //-colocando los valores que vienen de base de datos 
        tema.value = respuesta.perfilTema;
        desciption.value = respuesta.description;
        link.value = respuesta.linkeding;
        faceblink.value = respuesta.facebook;
        twiterlink.value = respuesta.twitter;
        weblink.value = respuesta.website;  
        avatar.src = respuesta.img;
        // return respuesta;  
 }


 async function getLikesAndNumberLogos(){ 
    var totalLikesInit = []; 
    let numerosLogos = document.getElementById('cant_logos');
    let allLikes = document.getElementById('cant_likes');
      
     const db = firebase.firestore(dbo);
     const conex = db.collection('CollectioLogotipo')
     .where("id_user", "==", getNameLs); 
     let querySnapshot = await conex.get();
     if(querySnapshot.docs.length !== 0){ 
        numerosLogos.innerText = querySnapshot.docs.length;
        querySnapshot.forEach(function(doc) {
            totalLikesInit.push(doc.data().like);
                sumLike(totalLikesInit, allLikes);
                filLogoBox(doc.data());
        });
    } 
}

 
function sumLike(totalLikes, likesContain ){  
let saldo = totalLikes.reduce(
    (prevValue, currentValue) => prevValue + currentValue,0);
  likesContain.innerText = saldo 
}




let filLogoBox = (datosCm) => { 
    let basegrid = document.getElementById('grid_box');
    let datosGet = datosCm;  
    let islok =  ()=>{
        if(datosGet.block === true){ 
          return (
            {
              block:lock ,
              estado: 'bloqueado',
              html: `<div id="actionUnlock">
                 <span id="lockCloseButon"> +</span>
                 <p>Coloque el pasw. de desbloqueo</p>
                 <div class="wrapperUnlok">
                 <input type="text" id="pw" /><button id="sendTounlock">desbloquear</button>
                 </div>
                 <b>si el pasw. es correcto estará desbloqueado por 10 minutos.</b>
           </div>`
            }
          )  
        }else{
          return (
           {
             block:open ,
             estado: 'desbloqueado',
             html: ``
           }
          )
        }
    }     


    let dwutlsvg = ()=>{
       if(datosGet.block === true){
           return "/" 
         }else{
           return  'href="'+ datosGet.imageSVG +'"';
         }
    }

    let dwutlpng = ()=>{
       if(datosGet.block === true){
           return "/" 
         }else{
           return  'href="'+ datosGet.imagePNG +'"';
         }
    }
     
    let dwurljpg = ()=>{
       if(datosGet.block === true){
           return "/" 
         }else{
           return  'href="'+ datosGet.imageJPG +'"';
         }
    }


   let putSVG = datosGet.imageSVG  ?  `<img  id="svgGet" src="`+svg+`"  alt="imgagen bajar png" />` : "";
   let putjpg = datosGet.imageJPG  ?  `<img  id="jpgGet" src="`+jpg+`"  alt="imgagen bajar png" />` : "";
   let putpng = datosGet.imagePNG  ?  `<img  id="pngGet" src="`+png+`"  alt="imgagen bajar png" />` : "";        
      

basegrid.innerHTML += ` 
<div class="logo_box `+datosGet.id_user+`" id="`+datosGet.id+`">  
<ul id="edit_delete"><li id="logEdt">editar logo</li><li class="`+datosGet.id+`" id="logDlt">borrar logo</li></ul>
<div id="logo_and_info"> 
  `+islok().html+`
  <div id="wrapper_info">
   <div class="logo_boxImg"><img src="`+ datosGet.miniature + `" alt="logo" /></div>
   <div class="logo_info">`+datosGet.Descripccion+`<p></p></div>
   
   </div>
</div>

<div class="title_box"><span>`+ datosGet.Nombre_logo + `</span></div>

<div class="action_box">
   <ul class="interactive">
       <li><a `+ dwutlsvg() +` download>`+ putSVG +`</a></li>
       <li><a `+ dwutlpng() +` download>`+ putpng +` </a> </li>
       <li><a `+ dwurljpg() +` download>`+ putjpg +`</a> </li>
       <li><img id="unlook" class="`+ islok().estado +`"  src=`+islok().block +`  alt="imgagen desbloquear" /> </li>
       <li id="getCounter"> 
           <ul>
           <li><span id="counter">`+datosGet.like+`</span></li>
           <li><img id="likeBox" class="`+datosGet.id+`" src="`+ like +`"  alt="dar like si os gusta"  /> </li>
       </ul>
           
       </li>
       <li><img id="infoGet" src= `+info+`  alt="obtener informacion" /> </li>
   </ul>
</div>
</div>`  
}


 

   useEffect(()=>{
     chargeData();   
     getLikesAndNumberLogos();
   },[]);
         
 
 

 

                         /* GRABAR LOS DATOS EN LA FITEBASE  */
   
         async function fixData(e){ 
    try{ 
      e.preventDefault();
        let getNameLs = localStorage.getItem('fileOptipus');
        const db = firebase.firestore(dbo);
        let getDatab = db.collection('usuarios').doc(getNameLs); 
        const env = await getDatab.set(datos,{merge:true});
        const dataGet = env;

    chargeData(); 
    console.log('datos guardados' , dataGet); 
    // console.log("dats: ",datos) 
  
    } catch (error){
        console.log("Resultados de un error al guardar: ",error) 
}

 
    } 



   function settextarea(e){
    let descResult = document.getElementById('userDescription');
     let text_area = e.target.value;
     descResult.innerHTML = text_area; 
     setdatos({...datos, description: text_area}) 
    } 

    function themeSwitc(e){  
        let btn = document.getElementById('sendBUTTOM'); 
        btn.disabled = true
        btn.classList.add('chargin')     

        const temaChange = e.target.value; 

        setdatos({...datos, perfilTema: temaChange})
        setInterval(()=>{
            let btn = document.getElementById('sendBUTTOM'); 
             btn.disabled = false
             btn.classList.remove('chargin') 
        }, 1000)
    }

    function linkeding(e){ 
        let innerlink = document.getElementById('link');
        if(e.target.value !== ""){
            setdatos({...datos, linkeding: e.target.value}); 
          innerlink.classList.remove('hideIt')
        }else{
            return
        }
    }
   
 
    function facebook(e){
        let innerFace = document.getElementById('face'); 
        if(e.target.value !== ""){
            setdatos({...datos, facebook: e.target.value}); 
            innerFace.classList.remove('hideIt')
        }else{
            return
        }
    }


    function twitterlink(e){
        let innerTWT = document.getElementById('twtt'); 
        if(e.target.value !== ""){
            setdatos({...datos, twitter: e.target.value}); 
            innerTWT.classList.remove('hideIt')
        }else{
          return
        }
        }  


        function websiteink(e){
            let innerWWW = document.getElementById('www'); 
            if(e.target.value !== ""){
                setdatos({...datos, website: e.target.value}); 
                innerWWW.classList.remove('hideIt')
            }else{
                return
            }
            }

                            /* subiendo la imagen */
            function upFl(fil){ debugger
                let getNameLs = localStorage.getItem('fileOptipus');
                const queviene = fil.target.files[0];
                let extencionIMG = queviene.type;
                let extDividida = extencionIMG.split('/')[1];
                const solo = queviene.name;
                  
                    // File deleted successfully
                    console.log("File deleted successfully");
                    var reader = new FileReader();
                    reader.onload = function(){
                    var dataURL = reader.result;
                    var output = document.getElementById('AvatImg');
                    var miniImg = document.getElementById('photoUser').children[0];
                    miniImg.src = dataURL;
                    output.src = dataURL;
                    };
                    reader.readAsDataURL(queviene);
                    //----=== salta la confirmacion para pasar a subir la imgaen
                    //---=== damos un segundo para que se cargue la imagen
                    setTimeout(()=>{ 
                        UpImageToFirestorage(getNameLs,getNameLs,extDividida,queviene); 
                },1000)        
            } 

            function UpImageToFirestorage(ruta,nombre,extencion,imagen){
            if (window.confirm("Esta seguro de usar esta imagen?")) { 
                const db = firebase.storage(dbo);
                const storag = db.ref(ruta+'/'+ nombre+'.'+extencion);
                const subio = storag.put(imagen)
                subio.on('state_changed',function(snapshot){ 
                    //--- Aqui podemos poner un preloader
                    }, function(error){
                        console.log("Error al subir imagen",error);
                    }, function(){
                        db.ref(ruta+'/').child(nombre+'.'+extencion).getDownloadURL()
                        .then(ur=>{

                            fixImageUrlToDatabase(ur, ruta);
                               
                        })
                })
                } else {
                console.log("You pressed Cancel!");
                var output = document.getElementById('AvatImg');
                output.src = datos.img;
                var miniImg = document.getElementById('photoUser').children[0];
                 miniImg.src = datos.img;
                }
            }



    let fixImageUrlToDatabase = (url, ruta)=>{
        const db = firebase.firestore(dbo);
        db.collection('usuarios').doc(ruta) 
            .set({img:url},{merge:true}).then(result=>{
            console.log('datos guardados' , result);
            }).catch(error=>{
                console.log("Resultados de un error al guardar: ",error)
            })
    }




if(localStorage.getItem('fileOptipus') !== null){
    return (
        <>
            
        <Header />
        
    <div id="perfil_usuario">
        <div className="perfil" style={estylos[datos.perfilTema]}>
            <div className="werrapper_perfil_data">
                <div id="edit_delete_avatar"><input id="temporal" onChange={upFl} type="file" name="avatar" accept="image/png, image/jpeg" />
                <img src={process.env.PUBLIC_URL + 'edit.svg'} alt="editar" />
                    </div>
                
                <img id="AvatImg" src={datos.img !== "" ? datos.img : "" } alt="avatar" />
            </div>
            <div className="wrapper_data">
                <span id="username"></span>    
                <p id="userDescription">{datos.description}</p>
                <span className="sigueme">Sigueme en:</span>
                <ul id="cajaRedes">
                    <li id="link" className={datos.linkeding === '' ? "hideIt" :'' }><a href={datos.linkeding} target="blank"><img src={process.env.PUBLIC_URL+"link.png"} alt="" /></a></li>
                    <li id="face" className={!datos.facebook ? "hideIt" :''}><a href={datos.facebook} target="blank"><img src={process.env.PUBLIC_URL+"faceb.png"} alt="" /></a></li>
                    <li id="twtt" className={!datos.twitter ? "hideIt" :''}><a href={datos.twitter} target="blank"><img src={process.env.PUBLIC_URL+"twtt.png"} alt="" /></a></li>
                    <li id="www" className={!datos.website ? "hideIt" :''}><a href={datos.website} target="blank"><img src={process.env.PUBLIC_URL+"www.png"} alt="" /></a></li>
                </ul>

                <ul id="likes">
                    <li className="your_likes"><img src={process.env.PUBLIC_URL+'like.svg'} alt="tus likes" /><b id="cant_likes"></b> :Likes Recibidos</li>
                    <li className="aportes"><img src={process.env.PUBLIC_URL+'up_logo.svg'} alt="tus likes" /><b id="cant_logos"></b> :Logos Subidos</li>
                    </ul>
            </div>

        </div>
        <div className="caja_acciones_perfil">
            <ul id="acciones">
            <li onClick={accionEdita}> Completar / Editar Perfil </li>
            <li onClick={accionLogo}> Tus Logos </li>
            </ul>
            <div id="formulario_compl_perfil" >
                <form id="compl_pf">
                    <label htmlFor="backgroun_perfil">Estilo de su perfil / tarda 7 seg.</label>
                    <select id="backgroun_perfil" onChange={themeSwitc}> 
                    <option value="no_selected">------------</option>
                    <option value="darklines">darklines</option>
                    <option value="circles">circles</option>
                    <option value="clear">clear</option>
                    <option value="difuse">difuse</option>
                    <option value="gaming">gaming</option>
                    <option value="tech">tech</option>
                    </select> 
                    
                    <label htmlFor="desc">Cuentanos sobre ti.</label>
                    <textarea onChange={settextarea} spellCheck="true" rows="6" id="desc" name="descripcion" pattern="[\d]"/>
                    <label htmlFor="linkeding">url: linkeding</label>
                    <input type="text" onChange={linkeding} id="linkeding" name="linkeding" placeholder="https://linkedin.com/nombre"/>
                    <label htmlFor="facebooklink">url: facebook</label>
                    <input type="text" onChange={facebook} id="facebooklink" name="facebook" placeholder="https://facebook.com/nombre"/>
                    <label htmlFor="twitterlink">url: twitter</label>
                    <input type="text" onChange={twitterlink}  id="twitterlink" name="twitter" placeholder="https://twitter.com/nombree"/>
                    <label htmlFor="websiteink">url: website</label>
                    <input type="text" onChange={websiteink}  id="websiteink" name="website" placeholder="https://website.com" />
                    <button id="sendBUTTOM" className=""  onClick={fixData}>Enviar</button>
                </form>
            </div>
            <div id="aportes" onClick={getActionInPerfil}>
            <h2>logotipos aportados</h2>
            <div className="wrapper_aportes">
            <div id="progressBar"></div>
            <section id="wrapper_grid">
    <div id="grid_box" >
    </div>
    </section>
            </div>
            </div>
        </div>
    </div> 
    </>      
    );
} else {
    window.location.href = '/';
    } 
}

 
export default Perfilform;