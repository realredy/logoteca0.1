import React, {useState } from 'react';
import Header from '../header';
import '../singIn/singIn.css';

import firebase from 'firebase';  
import Siguiente from '../SigPaso/SigPaso';
       

function herouseEffect(){
    const na = document.getElementById('loguinForm');  
    na.addEventListener("click", function(o){ 
    let spaner = o.target.nodeName;
    if(spaner === "INPUT"){
    let findSpan = o.target.previousSibling;
    findSpan.classList.add("peque"); 
    } else {
        console.log("bjeto no es un input")
    } 
    });  
    }
         
    
 
function Sing_in() { 
      const [infouser, setinfouser] = useState({
          activity: false,
          name: ""
        }); 
       

function getFormData(g){
g.preventDefault();
// let frmnode = document.getElementById("loguinForm");
let nombre = document.getElementById("namefrm") ;
let pasw = document.getElementById("passfrm") ;
let tels = document.getElementById("telfrm") ;
let mail = document.getElementById("mailfrm") ;
let em_p = document.getElementById("personaSl") ;
let msg = document.getElementById("message_error")

let n = {
email: mail.value, 
pass: pasw.value,
name: nombre.value, 
telef: tels.value, 
tipo: em_p.value 
};

let dign = n.name.replace(" ","")
	//   console.log(redBrd);
	var re = /[<>=+/*%!?@#$`^&;:(")'}{-]/g;
	var result = re.test(dign);
		if(result !== true){


            firebase.auth().createUserWithEmailAndPassword(n.email, n.pass)
    .then(datosuser=>{
    // Guardara los datos en la base
         if(datosuser !== ""){
            const db = firebase.firestore();
            const conex = db.collection('usuarios').doc(datosuser.user.uid);
            conex.set(n).then(data=>{   
                if(data !== ""){  
                    setinfouser({
                        activity:true,
                        name: n.name
                    });  
                }
            });
         } 
        }).catch(err=>{
        console.log(err)
        });




         }else{
            msg.innerHTML = `<button>Estos caracteres no estan permitidos en este renglón</button>`;
            nombre.style.borderBottom = "solid 3px red";
             
             setTimeout(()=>{
                msg.innerHTML = ``;
                nombre.style.borderBottom = "solid 1px #fbfbfb";
                
             },5000);
             
         }




};   

    return (
        <>
          
           <Header /> 
           <div id="form_singIn">
               <div className="wrapper_singIn">
                  <div className="left">
                      <form id="loguinForm" onSubmit={getFormData}>
                          <div className="sing_in_nombre">
                              <span>Nombre*</span>
                              <input onClick={herouseEffect} onFocus={herouseEffect} id="namefrm" type="text" name="nombre" required title="Coloque su nombre, No utilice carácteres especiales como +*/\%_>?<"/>
                          </div>
                          <div className="sing_in_pass">
                          <span>Pass*</span>
                              <input onClick={herouseEffect} onFocus={herouseEffect}  id="passfrm" type="password" name="pass" required pattern="[$+,:;=?@#|'<>{}.^*()%!-/[/ a-z0-9_]{6,}" title="su pasword debe tener al menos 6 items" />
                          </div>
                          <div className="sing_in_tel">
                          <span>Telef*</span>
                              <input onClick={herouseEffect} onFocus={herouseEffect}  id="telfrm" type="tel" name="pass" pattern="[0-9]{9,}" />
                          </div>
                          <div className="sing_in_email">
                          <span>Email*</span>
                              <input onClick={herouseEffect} onFocus={herouseEffect}  id="mailfrm" type="email" name="pass" required/>
                          </div>
                          <div className="sing_in_email">
                          <p>Empresa/Persona*</p>
                          <select id="personaSl" name="select_type">
                            <option value="private">---------</option> 
                            <option value="Empresa">Empresa</option>
                            <option value="Persona">Persona</option>
                            </select>
                          </div>
                          <input id="sendSignIn" type="submit" value="Realizar Subscripcion"/>
                          <div id="message_error"></div>
                      </form>
                  </div>
                  <div className="right">
                      <img src={process.env.PUBLIC_URL + 'creative_pr.jpg'} alt="proceso de creacion de logo" />
                  </div>
               </div>
               <div id="message_error"></div>
           </div>
           {infouser.activity ? <Siguiente dat={infouser.name} /> : " "}
        </>
    )
}

export default Sing_in
