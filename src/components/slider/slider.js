import React from 'react';  
import './slides.css';
 

function Slider() {
  return (
    <>
    <div className="slider" > 
    <div className="wrapperSlider">
        <h1>Que es logoteca y..</h1>
        <h2>Porque debo usar logoteca</h2>
        <p>Si tienes algun tipo de relacion con el mundo digital gráfico,
          sabrás que una de los reqisitos para producir, ya sea una app,
          un web site, o un arte digital, unos de los primeros recursos 
          a solicitar es el logo. El problema es en que formato os envian... 
          Ahora imagina que existe un lugar en donde ir por el formato correcto
          y los diferentes formatos que puedas necesitar.
        </p>
        <a href="/" alt="conocer mas">Conoce más..</a>
    </div>
      <div className="imgSlider"><img src={process.env.PUBLIC_URL + 'muestraPixelada.png'} alt="imagen slider" title="diferencia entre los formatos svg y jpg" /></div>
      
      </div>
      <div className="segundaSeccion">
        <div className="wrapperInfoslider">
          <b>Que es el formato SVG</b>
          <p>SVG es la abreviatura de Scalable Vector Graphics, 
            es decir, un formato de imagen de gráficos vectoriales. 
            Además este formato de archivo está basado en el lenguaje XML.
            Siendo este totalmente compartible con la web ademas de mantener 
            sus características sin pixelarse.
            </p>
            <a href="/">Conocer mas</a>
          </div>
      <img src={process.env.PUBLIC_URL + 'svgImportant.jpg'} alt="imagen slider" title="mas sobre svg" />
      </div>
      <div className="seguridad">
      <div className="Wrapper_seguridad">
      <img src={process.env.PUBLIC_URL+ 'relax.svg'} />
      <div className="infoSeguridad">
        <h3>Relax. Tienes opción de seguridad.</h3>
        <p>Sabemos que existen logos que no querras que todos tengan acceso a el,
          por lo tanto tenemos un sistema de seguridad de doble accion en donde 
          puedes o ceder el pasword o desbloquear el logo por 10 Segundos, todo esto 
          con un password encriptado.
        </p>
        <a href="/">Quieres saber mas...</a>
        </div>
        
      </div>
      </div>
      <div id="pasos_paraSubir_logo">
        <img src={process.env.PUBLIC_URL+ 'uploadLogo.svg'} />
        <img src={process.env.PUBLIC_URL+ 'build.svg'} />
                </div>
      </>
  
  );
}

export default Slider;
