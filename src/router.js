 import React from 'react';
 import {BrowserRouter, Route} from 'react-router-dom';
import home from './components/home/home';
import sing_in from './components/singIn/sing_in';
import Perfilform from './components/perfil/perfilForm';
  import Subirlogo from './components/SubirLogo/Subirlogo';
import Boxlogotipo from './components/Sectionlogos/Boxlogotipo';
import Testreducer from './components/Testreducer/Testreducer';
 
 
 function router(){
 
     return (
         <BrowserRouter>
             
             <Route exact path="/" component={home} />
             <Route path="/singin" component={sing_in} />
             <Route path="/gridlogos" component={Boxlogotipo} />
             <Route path="/subirlogo" component={Subirlogo} />
            <Route path="/perfil" component={Perfilform} />  
             <Route path="/testy" component={Testreducer} />
             
         </BrowserRouter>
     )
   }
 export default router;
 